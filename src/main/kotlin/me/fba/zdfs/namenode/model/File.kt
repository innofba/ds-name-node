package me.fba.zdfs.namenode.model

import org.springframework.data.annotation.Id
import org.springframework.data.redis.core.RedisHash

@RedisHash("files")
data class File(@Id val path: String, var content: List<String>, var type: String)