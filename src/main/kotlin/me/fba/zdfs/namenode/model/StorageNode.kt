package me.fba.zdfs.namenode.model

import com.sun.org.apache.xpath.internal.operations.Bool
import org.springframework.core.io.FileSystemResource
import org.springframework.http.*
import org.springframework.util.LinkedMultiValueMap
import org.springframework.web.client.RestTemplate
import java.lang.Exception
import java.nio.file.Files
import java.nio.file.Path
import java.security.MessageDigest
import java.util.*


class StorageNode(
        val id: String,
        val url: String
) {
    fun ping(): Boolean {
        return try {
            RestTemplate().getForEntity("$url/ping", String::class.java).statusCode == HttpStatus.OK
        } catch (e: Exception) {
            false
        }
    }

    fun availableSpace(): Long = RestTemplate().getForObject("$url/availableSpace", String::class.java)!!.toLong()

    fun upload(file: Path, hash: String) {
        val bodyMap = LinkedMultiValueMap<String, Any>()
        bodyMap.add("file", FileSystemResource(file.toFile()))
        val headers = HttpHeaders()
        headers.contentType = MediaType.MULTIPART_FORM_DATA
        val requestEntity = HttpEntity(bodyMap, headers)

        RestTemplate().exchange("$url/file/$hash",
                HttpMethod.POST, requestEntity, String::class.java)
    }

    fun rm(hash: String) = RestTemplate().delete("$url/file/$hash")

    fun init() = RestTemplate().getForEntity("$url/init", String::class.java)

    fun contains(hash: String) = try {
        RestTemplate().headForHeaders("$url/file/$hash")
        true
    } catch (e: Exception) {
        false
    }
}