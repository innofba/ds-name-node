package me.fba.zdfs.namenode.rest

import me.fba.zdfs.namenode.model.File
import me.fba.zdfs.namenode.service.FileService
import me.fba.zdfs.namenode.service.StorageNodeService
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.servlet.view.RedirectView

@RestController
class FileController(
        val fileService: FileService,
        val storageNodeService: StorageNodeService
) {

    @RequestMapping("/availableSpace")
    fun availableSpace() = storageNodeService.availableSpace()

    @RequestMapping("/ls")
    fun ls(@RequestParam path: String) = fileService.ls(path)

    @RequestMapping("/mkdir")
    fun mkdir(@RequestParam path: String) = fileService.mkdir(path)

    @RequestMapping("/rm")
    fun rm(@RequestParam path: String) = fileService.rm(path)

    @RequestMapping("/upload")
    fun upload(@RequestParam file: MultipartFile, @RequestParam path: String) = fileService.save(path, file)

    @RequestMapping("/get")
    fun get(@RequestParam path: String): RedirectView = RedirectView(fileService.getLocation(path))

    @RequestMapping("/mv")
    fun mv(@RequestParam from: String, @RequestParam to: String) = fileService.mv(from, to)

    @RequestMapping("/cp")
    fun cp(@RequestParam from: String, @RequestParam to: String) = fileService.cp(from, to)

    @RequestMapping("/init")
    fun init() = fileService.init()

    @RequestMapping("/info")
    fun info(@RequestParam path: String): File = fileService.get(path)
            .orElseThrow { FileService.NoSuchFileOrDirectoryException() }

    @RequestMapping("/storagenode")
    fun registerStorageNode(@RequestParam id: String, @RequestParam publicUrl: String) = storageNodeService.registerStorage(id, publicUrl)

    @RequestMapping("/storagenode/file/all")
    fun sync(): List<Map<String, String>> = fileService.fileRepository.findAll()
            .filter { it.type == "zdfs/file" }
            .map {
                val map = HashMap<String, String>()
                map["filename"] = it.path
                map["hash"] = it.content[0]
                map["url"] = storageNodeService.getFileLocation(it.content)
                return@map map
            }

}