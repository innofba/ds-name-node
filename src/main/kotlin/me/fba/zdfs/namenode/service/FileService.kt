package me.fba.zdfs.namenode.service

import me.fba.zdfs.namenode.model.File
import me.fba.zdfs.namenode.repository.FileRepository
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.multipart.MultipartFile
import java.lang.RuntimeException
import java.nio.file.Files
import java.security.MessageDigest
import javax.annotation.PostConstruct

@Service
class FileService(
        val fileRepository: FileRepository,
        val storageNodeService: StorageNodeService
) {

    private val directoryType = "zdfs/directory"
    private val fileType = "zdfs/file"

    fun get(path: String) = fileRepository.findById(path)

    fun ls(path: String): Iterable<File> = get(path)
            .filter { it.type == directoryType }
            .map { fileRepository.findAllById(it.content) }
            .orElseThrow { return@orElseThrow NoSuchFileOrDirectoryException() }
            .map {
                if (it.type != fileType)
                    return@map it

                val nodes = storageNodeService.nodes.values.filter { n -> n.contains(it.content[0]) }
                val content = listOf<String>(it.content[0], it.content[1], nodes.joinToString { it.id })
                File(it.path, content, it.type)
            }

    fun getLocation(path: String): String {
        if (get(path).map { it.type != fileType }.orElse(true))
            throw NoSuchFileOrDirectoryException("File not found")

        return storageNodeService.getFileLocation(get(path).get().content)
    }

    fun exists(path: String) = get(path).isPresent

    fun mkdir(path: String) = touch(path, type = directoryType)

    @Transactional
    fun touch(path: String, content: List<String> = listOf(".zdfs"), type: String) {
        if (exists(path))
            throw FileAlreadyExistsException()

        fileRepository.save(File(path, content, type))

        val dir = get(getDirectory(path))
                .orElseThrow { return@orElseThrow NoSuchFileOrDirectoryException() }

        val dirContent = dir.content.toMutableList()
        dirContent.add(path)

        dir.content = dirContent

        fileRepository.save(dir)
    }

    fun save(path: String, file: MultipartFile) {
        if (exists(path))
            throw FileAlreadyExistsException()

        if (!exists(getDirectory(path)))
            throw NoSuchFileOrDirectoryException("No such directory")

        touch(path, storageNodeService.upload(file, fileRepository.findAll()), fileType)
    }

    @Transactional
    fun rm(path: String, removeFromStorage: Boolean = true) {
        if (path == "/")
            throw NoSuchFileOrDirectoryException()

        val file = get(path)
                .orElseThrow { return@orElseThrow NoSuchFileOrDirectoryException() }

        if (file.type == directoryType && ls(path).count() > 0)
            throw DirectoryNotEmptyException()

        val dir = get(getDirectory(path))
                .orElseThrow { return@orElseThrow NoSuchFileOrDirectoryException() }

        dir.content = dir.content.filter { it != path }

        if (removeFromStorage && file.type == fileType) {
            fileRepository.findAll()
                    .filter { it.type == fileType }
                    .filter { it.path != file.path }
                    .filter { it.content[0] == file.content[0] }
                    .ifEmpty { storageNodeService.rm(file) }
        }

        fileRepository.save(dir)

        fileRepository.deleteById(path)
    }

    fun mv(from: String, to: String, removeSource: Boolean = true) {
        if (!exists(from) || !exists(getDirectory(to)))
            throw NoSuchFileOrDirectoryException()

        if (exists(to))
            throw FileAlreadyExistsException()

        val nodes = get(from).get()

        if (nodes.type == directoryType)
            throw DirectoryNotMovableException()

        if (removeSource)
            rm(from, false)

        touch(to, nodes.content, nodes.type)
    }

    fun cp(from: String, to: String) = mv(from, to, false)

    fun init() {
        fileRepository.findAll().forEach { fileRepository.deleteById(it.path) }
        setUp()
        storageNodeService.init()
    }

    @PostConstruct
    private fun setUp() {
        if (!fileRepository.findById("/").isPresent)
            fileRepository.save(File("/", listOf(".zdfs"), directoryType))
    }

    private fun getDirectory(path: String) = if (path.lastIndexOf('/') == 0) "/" else path.substringBeforeLast('/')

    @ResponseStatus(HttpStatus.NOT_FOUND)
    class NoSuchFileOrDirectoryException(msg: String = "No such file or directory") : RuntimeException(msg)

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    class FileAlreadyExistsException(msg: String = "File already exists") : RuntimeException(msg)

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    class DirectoryNotEmptyException(msg: String = "Directory is not empty") : RuntimeException(msg)

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    class DirectoryNotMovableException(msg: String = "Directory is not movable") : RuntimeException(msg)
}