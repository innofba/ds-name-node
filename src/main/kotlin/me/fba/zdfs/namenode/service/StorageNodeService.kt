package me.fba.zdfs.namenode.service

import me.fba.zdfs.namenode.model.File
import me.fba.zdfs.namenode.model.StorageNode
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.lang.Exception
import java.nio.file.Files
import java.security.MessageDigest
import java.util.*
import java.util.concurrent.ConcurrentHashMap

@Service
class StorageNodeService {

    private val log = LoggerFactory.getLogger(StorageNodeService::class.java)

    val nodes = ConcurrentHashMap<String, StorageNode>()

    fun get(nodeId: String): StorageNode? = nodes[nodeId]

    fun getAllByAvailableSpace(availableSpaceBytes: Long) = nodes.values
            .filter { it.ping() }
            .filter { it.availableSpace() >= availableSpaceBytes }
            .shuffled()

    fun upload(file: MultipartFile, files: Iterable<File>): List<String> {
        val tempFile = Files.createTempFile("zdfs", "tmpfile")
        file.transferTo(tempFile)

        val fileHash = Base64.getUrlEncoder().encodeToString(
                MessageDigest.getInstance("SHA-512").digest(Files.readAllBytes(tempFile)))
        val fileSize = Files.size(tempFile)

        val alreadyStordFile = files.find { it.content[0] == fileHash }
        if (alreadyStordFile != null)
            return alreadyStordFile.content

        val locs = getAllByAvailableSpace(fileSize)

        locs.forEach { it.upload(tempFile, fileHash) }

        Files.delete(tempFile)

        return listOf(fileHash, fileSize.toString(), locs.joinToString { it.id })
    }

    fun availableSpace() = nodes.values
            .filter { it.ping() }
            .map { Triple(it.id, it.url, it.availableSpace()) }

    fun getFileLocation(metadata: List<String>): String {
        val hash = metadata[0]

        val storageNode = nodes.values
                .filter { it.contains(hash) }
                .shuffled()[0]

        return "${storageNode.url}/file/$hash"
    }

    fun init() = nodes.values.filter { it.ping() }
            .forEach {
                try {
                    it.init()
                } catch (e: Exception) {
                    log.debug("Failed to init storage node url:${it.url}")
                }
            }

    fun rm(file: File) {
        val hash = file.content[0]
        val nodeIds = file.content[2].split(", ")

        nodeIds.mapNotNull { get(it) }
                .filter { it.ping() }
                .forEach { it.rm(hash) }
    }

    fun registerStorage(id: String, url: String) {
        val node = StorageNode(id, url)

        if (nodes[id]?.url == node.url)
            return

        if (!node.ping()) {
            log.error("Discovered storage node is not reachable $url")
            return
        }

        val previous = nodes.put(id, node)
        if (previous == null)
            log.info("Registered storage node $id $url")
        else
            log.info("Updated address for node id:$id new:${node.url} old:${previous.url}")
    }

}