package me.fba.zdfs.namenode.repository

import me.fba.zdfs.namenode.model.File
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface FileRepository : CrudRepository<File, String> {
}