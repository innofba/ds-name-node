package me.fba.zdfs.namenode.discovery

import me.fba.zdfs.namenode.service.StorageNodeService
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.net.DatagramPacket
import java.net.InetAddress
import java.net.MulticastSocket
import java.net.NetworkInterface
import java.net.DatagramSocket
import javax.annotation.PostConstruct


@Service
class BroadcastReceive(
        val storageNodeService: StorageNodeService
) {

    private val log = LoggerFactory.getLogger(BroadcastReceive::class.java)

    fun run() {
        val socket = DatagramSocket(37020, InetAddress.getByName("0.0.0.0"))
        socket.broadcast = true
        val buf = ByteArray(512)
        val packet = DatagramPacket(buf, 512)
        while (true) {
            socket.receive(packet)
            val msg = String(packet.data.sliceArray(IntRange(0, packet.length - 1)))
            val host = packet.address.hostAddress
            onMessage(host, msg)
        }
    }

    private fun onMessage(host: String, message: String) {
        log.debug("BROADCAST <- $host: $message")

        val split = message.split("|")
        if (split.size == 5 && split[0] == "zdfs/storage" && split[4] == "zdfs")
            storageNodeService.registerStorage(split[2], split[3])
    }

    @PostConstruct
    fun setUp() {
        Thread {run()}.start()
    }
}