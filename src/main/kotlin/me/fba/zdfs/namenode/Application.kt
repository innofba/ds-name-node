package me.fba.zdfs.namenode

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory
import org.springframework.data.redis.connection.RedisStandaloneConfiguration



@SpringBootApplication
class NameNodeApplication(
        @Value("\${REDIS_HOST:localhost}") val redisHost: String,
        @Value("\${REDIS_PORT:6379}") val redisPort: Int
) {
    val log = LoggerFactory.getLogger(NameNodeApplication::class.java)

    @Bean
    fun redisConnectionFactory(): JedisConnectionFactory {
        log.info("Configuring redis on $redisHost:$redisPort")
        return JedisConnectionFactory(RedisStandaloneConfiguration(redisHost, redisPort))
    }
}

fun main(args: Array<String>) {
    runApplication<NameNodeApplication>(*args)
}
